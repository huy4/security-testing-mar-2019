# Buổi 9
## Tổng quan
- A8:2017-Insecure Deserialization
- A9:2017-Using Components with Known Vulnerabilities
- A10:2017-Insufficient Logging & Monitoring
## Chi tiết
- A8:2017-Insecure Deserialization
	- Serialization: là quá trình lưu object vào file
	- Deserialization: là quá trình ngược lại đọc từ file ra object
	![Serialization and Deserialzation](./assets/images/A9 Insecure Deserialzation.PNG)
	- Tấn công:
		- Hacker bằng cách chèn function chứa đoạn mã độc vào object
		- Quá trình xử lý serialization và deserialization không tốt thì lúc load dữ liệu thì function sẽ được thực thi
	- Đặc điểm:
		- Ít phổ biến
	- Các test:
		- Review code hoặc test white box
- A9:2017-Using Components with Known Vulnerabilities
	- Sử dụng các component đã chứa lỗi thông dụng
	- Các test:
		- Liệt kê ra các library, component và version
		- Sử dụng website [Exploit Database](https://www.exploit-db.com/) library, component có lỗi hay không
	- Recommended:
		- Update
		- Remove hoặc disable các component có lỗi hoặc không sử dụng
- A10:2017-Insufficient Logging & Monitoring
	- Ghi lại các sự kiện khi user sử dụng hệ thống
	- Những ý chính khi lưu log:
		- Ghi lại sự kiện user login sai nhiều lần
		- Sao lưu các log lên cloud
		- Cấu hình firewalls và routing
		- Khi bị tấn công thì nên có thông báo qua email hoặc sms
		- Thông báo gửi về nên real time
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/13vWb3fLvD1khgIapfctyNa3UGzAU42hY/view?usp=sharing)