# Buổi 4
## Tổng quan
- A2: Broken Authentication
- Tìm hiểu và thực hành về Authentication Checklist

## Chi tiết
- Broken Authentication
	- Liên quan đến `authentication` xác thực và `session management` quản lý session
- Authentication Checklist
	- Permits brute force or other automated attacks. No wrong password limit
	- Permits default, weak, or well-known passwords
	- Uses plain text, encrypted, or weakly hashed passwords
	- Exposes Session IDs in the URL
	- Does not rotate Session IDs after successful login
	- Does not properly invalidate Session IDs.
	- Indicate the username or password that was wrong when the login attempt fails
	- Weak password change controls
- Trong các checklist trên sẽ chú ý với các bài test sau:
	- Permits brute force or other automated attacks. No wrong password limit
	-> Ngăn chặn hacker dùng phương pháp thử sai với khi đăng nhập
	- Does not rotate Session IDs after successful login
	-> Nên tạo session ID mới khi đăng nhập thành công
	- Does not properly invalidate Session IDs.
	-> Hủy session ID khi người dùng đăng xuất.
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/11kMcg0BzK7eIj7AwXuETgfbp4zMDlr_V/view?usp=sharing)