# Buổi 1

## Tổng quan
- Introducing
- Security Midset
- Intercept & Modify Request
- Setup BurpSuite & Browser
- Hands on Labs: Bypass the client side validation

## Chi tiết
- Security Midset
	- Vấn đề (issue) nhìn từ góc Security là rủi ro (risk) không phải bug
	- Rủi ro (risk) có thể xảy ra hoặc không
- Intercept & Modify Request
	- Mô hình Client và Server side
	- Client side môi trường được sử dụng để chạy các đoạn mã trên trình duyệt
	- Server side môi trường để chạy các đoạn mã ở server
- Setup BurpSuite & Browser
	- BurpSuite là phần mềm để kiểm thử xâm nhập ứng dụng web
	- Browser được dùng là Firefox
- Hands on Labs: Bypass the client side validation
	- Từ phần mềm BurpSuite chọn Proxy -> Option để xem IP:Port
	- Cài đặt Proxy tại firefox bằng cách vào Menu -> Option/Referrence -> Tìm kiếm proxy
	- Tại cửa sổ proxy mới hiển thị lên chọn option: Manual proxy configuration điền ip vào HTTP Proxy và port xem ở BurpSuite
	- Lựa chọn vào `Use this proxy server for all protocols`
	- Export & Import Certificate bằng BurpSuite lựa chọn proxy -> options
	- Chọn nút `Import/ export CA certificate`, tại format Export chọn Certificate in DER format
	- Export file có đặt tên là burp.cer
	- Mở trình duyệt firefox -> option/referrence -> tìm kiếm từ khóa certificate -> view certificate > import certificate burp.cer
	- Tại phần mềm BurpSuite chọn tab Proxy và Intercept chuyển sang chế độ Intercept off
	Kịch bản thử nghiệm
	Thử nghiệm với trang web: http://zero.webappsecurity.com/feedback.html
	1. Submit tất các trường theo đúng yêu cầu
	2. Bắt các request, bỏ trống trường name
	3. Submit to Server
	Trường hợp 1: Server Error -> name không thể bỏ trống -> pass
	Trường hợp 2: Success -> fail

## Đính kèm
- Tải BurpSuite [tại đây](https://portswigger.net/burp/communitydownload)
- Tải Browser Firefox [tại đây](https://www.mozilla.org/en-US/firefox/new/)
- Tài liệu [Intercept & Modify Request.pdf](https://docs.google.com/spreadsheets/d/18W7gRlLu18KnumoT0bfoOqUnA6MaGZfYPqX7j4FOSko/edit?usp=sharing)