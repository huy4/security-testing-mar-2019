# Buổi 12
## Tổng quan
- Tìm hiểu thêm một số chức năng của Burp suite
## Chi tiết
- Comparer
	- So sánh các giá trị của request hoặc response
	- Cách dùng:
		- Đến từng phương thức chọn `Send to Comparer (response)`
		- Chuyển sang tab `Comparer` và chọn words
- Decode/ Encode
	- Send từ request hoặc response sang `Decoder`
	- Bôi đen nội dung cần `Encode` hoặc `Decoder`
- Sử dụng Intruder
	- Sử dụng `Payload Sets` để chọn cách tấn công như `Brute force`,...
	- `Payload Options [Simple list]` để thêm payload
	- `Payload Processing` bổ sung cho phần `Payload Options [Simple list]` như Encode hoặc Decode,...
- Tab `Target`
	- Sử dụng `Add to scope` để thêm các domain vào các scope riêng để scan
- Tab `Proxy` -> `Intercepts`
	- Kích hoạt `Intercepts is on`
	- Mục đích: Phân tích request
	- Phân tích các request bằng cách đọc từng bước thực hiện request bằng cách bấm `Forward`
## Đính kèm