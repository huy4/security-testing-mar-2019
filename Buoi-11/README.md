# Buổi 11
## Tổng quan
- A10 (2013) – Unvalidated Redirects and Forwards
## Chi tiết
- A10 (2013) – Unvalidated Redirects and Forwards
	- Đánh lừa user bằng cách gửi request đã sửa đổi điều hướng sang trang chứa mã độc
	- Đặc điểm:
		- Tỷ lệ: Không phổ biến
		- Phát hiện: Dễ dàng
	- Ngăn chặn:
		- Hạn chế sử dụng điều hướng và chuyển tiếp
		- Nếu sử dụng điều hướng ra trang bên ngoài không thuộc website thì nên xác nhận với người dùng
		- Đảm bảo các tham số truyền vào dùng để diều hướng là hợp lệ và có xác nhận từ người dùng
		- Tạo một danh sách các trang tin tưởng cho phép điều hướng
## Đính kèm