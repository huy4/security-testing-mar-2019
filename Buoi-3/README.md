# Buổi 3
## Tổng quan
- Tìm hiểu về OWASP Top 10
- A1:2017-Injection

## Chi tiết
- Danh sách OWASP Top 10 <br>
  	![OWASP Top 10](./assets/images/OWASP-top-10.png)
- A1: Injection
	- Tấn công Injection là tấn công chèn mã độc trong câu truy vấn
	- Đây là hình thức tấn công được xếp hạng nguy hiểm vì:
		- Nó có thể qua được xác thực
		- Thêm, sửa, xóa hoặc đọc nội dung trong cơ sở dữ liệu
		- Thực hiện với quyền người quản trị với cơ sở dữ liệu như crop table,...
	- Cách thức phòng chống:
		- Chuẩn bị các câu truy vấn có sẵn, hạn chế nhận dữ liệu từ user nhập dữ liệu vào
		- Viết sẵn các câu truy vấn để lúc cần gọi lên dùng
		- Không nhận các ký tự liên quan đến Injection
		- Chuyển đổi các dấu nháy đơn, ... liên quan đến Injection thành các ký tự bình thường thay thế
	- Cách kiểm tra trang web có chứa lỗ hổng có thể tấn công Injection
		- Bước 1: Nhập tên đăng nhập và mật khẩu bất kỳ và gửi
		- Bước 2: Tại Burpsuite bắt sự kiện đăng nhập gửi lên và thao tác các bước như sau: <br>
		![Kiem tra tan cong Injection](./assets/images/buoc-1.png) <br>
		![Kiem tra tan cong Injection](./assets/images/buoc-2.png) <br>
		Chọn trường cần tấn công
		![Kiem tra tan cong Injection](./assets/images/buoc-3.png) <br>
		Thực hiện với từng câu truy vấn Injection đọc từ file `sql-injection-list.txt`
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/10x3k5Rg4svHcU4aM9w2wTTGYsn6pPzf_/view?usp=sharing)
- [sql-injection-list.txt](./assets/file/sql-injection-list.txt)