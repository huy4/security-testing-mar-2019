# Buổi 4
## Tổng quan
- Setup OWASP Mutillidae (practice website)
- A3:2017-Sensitive Data Exposure
- A4:2017-XML External Entities (XXE)

## Chi tiết
- Setup [Xampp](https://www.apachefriends.org/download.html) và [Mutillidae](https://sourceforge.net/projects/mutillidae/)
	- Copy thư mục Mutillidae vào theo đường dẫn `C:\xampp\htdocs\`
	- Vào thư mục `C:\xampp\htdocs\mutillidae\includes` mở file `database-config.php` và để trống trường `DB_PASSWORD`
	- Chạy Mutillidae bằng cách bật Apache và MySQL ở Xampp
	- Vào đường dẫn 127.0.0.1:[port]/mutillidae
- A3:2017-Sensitive Data Exposure - Lộ thông tin nhạy cảm
	- Là ứng dụng không bảo vệ được các thông tin cần bảo vệ
	- Chia thành 2 nhóm thông tin:
		- Thông tin về người dùng trong Authentication:
			- Credentials
			- PINs
			- Session identifiers
			- Tokens, Cookies
		- Thông tin được bảo vệ bởi điều khoản, quy định hoặc chính sách cụ thể:
			- Credit Cards
			- Customers data
	- Severity: Dựa vào thông tin bị lộ ra ngoài
	- Lỗ hổng này khó được phát hiện do:
		- Tool khó phát hiện -> Khó test
		- External test: không biết cụ thể data nhạy cảm được lưu xuống đã mã hoá hay chưa
	- Một vài ví dụ về risk Sensitive Data Exposure:
		- Nội dung trong file robots.txt không được xử lý kỹ
		- Thông tin phiên bản máy chủ sử dụng như nginx 1.1
- A4:2017-XML External Entities (XXE)
	- Định nghĩa về XML:
		- XML được viết tắt eXtensible Markup Language.
		- XML được thiết kế để chuyển dữ liệu
		- XML được thiết kế để cả người dùng và máy đều dễ hiểu
	- Đây là dạng tấn công dựa vào các ứng dụng web sử dụng XML và không xử lý các dữ liệu gửi lên từ trình duyệt
	- Tấn công XXE cũng gần giống tấn công Injection nhưng khác nhau `Payloads` gửi lên
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/16UnrNgC42G-u3ZivOy0lBVd5Wmn7cSaJ/view?usp=sharing)