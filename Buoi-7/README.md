# Buổi 7
## Tổng quan
- A7:2017-Cross-Site Scripting (XSS)
- Using Xenotix

## Chi tiết
- A7:2017-Cross-Site Scripting (XSS)
	- XSS xảy ra khi không xử lý input, hacker lợi dụng chèn các đoạn mã script độc vào cơ sở dữ liệu hoặc lấy Session ID
	- Đặc điểm:
		- Phổ biến
		- Khó nhận diện và gỡ bỏ
		- Đánh cắp thông tin người dùng hoặc cookies
	- Cách kiểm tra:
		- Nhập đoạn mã <script>alert('test')</script> vào URL và các ô input để kiểm tra
	- Phân loại:
		- Non persistent (Reflected XSS): Chèn mã độc vào URL - Đây là các hacker có thể gửi link có chứa đoạn script mã độc để đánh lừa người dùng khi click vào đường link
		- Persistent (Stored XSS): Hacker chèn các đoạn mã script vào input hoặc ô bình luận của các bài viết và đoạn mã độc sẽ được lưu vào cơ sở dữ liệu, bất cứ người dùng nào ghé qua bài đọc đều dính mã độc, đây là hình thức tấn công được đánh giá nguy hiểm
		- DOM base XSS: Chèn các cây DOM HTML để đánh lừa người dùng nhập dữ liệu vào phần hacker thêm vào
	- Ngăn chặn:
		- Lọc các tham số truyền vào từ input như không nhận các giá trị giống những đoạn mã script của người dùng nhập vào
		- Lọc các tham số truyền ra như không render các đoạn mã script của người dùng nhập vào từ cơ sở dữ liệu
		- Encode những đoạn mã script
- Using Xenotix
	- Bật server
	- Sử dụng phương thức GET:
		- Nhập một đoạn ký tự bất kỳ và xem URL có chứa đoạn ký tự đó không?
		- Nếu có thì thay đúng đoạn ký tự đó bằng [X]
		- Ấn puzz để bắt đầu quét
	- Sử dụng phương thức POST:
		- Nhập một đoạn ký tự bất kỳ và bắt request POST trong Inspect và tab network
		- Chọn Edit and Resend lấy URL và giá trị của ô Request body
		- Paste URL vào input URL, Request body vào Parameters
		- Tại Parameters tìm đến đoạn ký tự vừa gửi lên thay thế bằng [X]
		- Ấn puzz để bắt đầu quét
	- Khi bắt được những popup gửi hiển thị lên tức là website fail ở bài test XSS
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/17rwAI2xINPDxjnwibJtw03zJGHpoTq0V/view?usp=sharing)