# Buổi 8
## Tổng quan
- Review Session + Security Test Plan
## Chi tiết
- Review Session
	- SQL Injection
	- Broken Authentication
		- Bổ sung ý đã quên:
		- Does not rotate Session IDs after successful login
			- Không tạo các Session ID mới sau khi đăng nhập thành công
		- Does not properly invalidate Session IDs
			- Không xóa các Session ID khi người dùng đã đăng xuất
	- Sensitive Data Exposure
	- XML External Entities (XXE)
	- Broken Access Control
	- Security Misconfiguration
		1. Unpatched security flaws in the server software.
		2. Improper file and directory permissions.
		3. Unnecessary services in enabled state.
		4. Default accounts with their default passwords.
		5. Exposure of administrative or debugging notifications to general users.
		6. Misconfigured SSL certificates and encryption settings.
			- Nếu trường hợp website sử dụng phương thức HTTP thì website fail ở bài test này
			- Nếu trường hợp website sử dụng phương thức HTTPs thì tiếp tục check tại trang https://www.ssllabs.com/ssltest/ điểm chấm từ B trở xuống thì cũng đánh giá là fail ở bài test này
		7. Misconfiguration of user roles.
	- Cross-Site Scripting
		- Non-persistent (Reflected XSS)
		- Persistent (Stored XSS)
		- DOM Based XSS
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/17rwAI2xINPDxjnwibJtw03zJGHpoTq0V/view?usp=sharing)
- Security Test Plan [Download here](https://drive.google.com/file/d/18fyhYaAtdhkNojHjZjz7mIu95lCuQGMZ/view?usp=sharing)