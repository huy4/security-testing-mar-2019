# Buổi 2
## Tổng quan
- Security Test Report
- Security Test Reproduce
## Chi tiết
- Security Test Report
	- Có thể report được các issue theo security format.
		- Title: Miêu tả ngắn gọn rủi ro (risk)
		- Details:	Miêu tả chi tiết rủi ro
		- Severity: Mức độ của rủi ro
		- Steps: Tái hiện lại từng bước cho đến khi gặp rủi ro
		- Request: Phần request gửi lên bắt được ở BurpSuite
		- Response: Kết quả nhận về của request bắt được BurpSuite
		- Recommended: Mong muốn rủi ro được fix như thế nào?
		- Screenshot: Chụp ảnh màn hình request và response từ phần mềm BurpSuite
	- Note:
		- Severity:
    		- Risk = Impact * Likelihood
			- Impact: Là khả năng tác động của rủi ro
			- Likelihood: Là khả năng hacker có thể tấn công khi khai thác rủi ro vừa tìm được
    - Hình minh họa:
  
		![Overall Risk Reserity](./assets/images/severity.PNG)
		
- Security Test Reproduce
	- Có thể đọc hiểu, tái tạo lại thành công một report (từ tool xuất ra hoặc của một tester khác) theo phương thức GET/ POST
	- Hiểu cách hoạt động của Request & Response
	- Từng bước:
		- Đọc từ trường `host` để chọn địa chỉ
		- Xem tiếp ngay bên cạnh phương thức GET/POST là phần tiếp theo của đường dẫn
		- Truy cập đường dẫn và bắt request bằng BurpSuite
		- Chắc chắn rằng request bắt được từ BurpSuite và request từ report giống nhau
		- Chọn nút `Go` để bắt được đúng response và so sánh response từ report
		- Viết report về rủi ro theo mẫu Security Test Report
	
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/12LQXk3YHhDCMWQTfyXNl39X2L0u5odBy/view?usp=sharing)
- Format request và Response [plantB](http://www.planetb.ca/syntax-highlight-word)
- [Thực hành theo bài giảng](https://docs.google.com/document/d/1CenTA17SS-RikUiU-xNCwRYNTdLYlNHp7AjRqB8DPfE/edit?usp=sharing)