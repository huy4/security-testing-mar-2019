# Buổi 4
## Tổng quan
- A5:2017-Broken Access Control
- A6:2017-Security Misconfiguration
- Using ZAP tool (Part 2)

## Chi tiết
- A5:2017-Broken Access Control
	- Là rủi ro liên quan đến không quản lý tốt quyền của người dùng.
	- Cụ thể là người dùng này có thể xem được hoặc sửa thông tin của người dùng kia có cùng role hoặc cao hơn
	- Có 2 kịch bản chính:
		- Tấn công bằng cách thay đổi tham số trong đường dẫn URL
		- Tấn công bằng cách đổi một số trường trong body
	- Severity: Tùy thuộc vào mức độ quan trọng của thông tin bị lộ ra ngoài hoặc trường bị thay đổi
	- Recommended: Xây dựng chặt chẽ quyền cho người dùng
- A6:2017-Security Misconfiguration
	- Đây là rủi ro liên quan đến việc cấu hình, cài đặt của website
	- 7 lỗi thường gặp nhất như sau:
		1. Unpatched security flaws in the server software.
			- Ứng dụng dùng những library, components chứa lỗ hổng, có thể là do version đã cũ
			- Người dùng nên cập nhật library, components những bản mới nhất
		2. Improper file and directory permissions.
			- Quyền truy cập vào các file và đường dẫn quan trọng
			- Ví dụ như về file robots.txt
			- Nên đặt quyền truy cập những file và đường dẫn quan trọng cho admin hoặc tài khoản có role cao
		3. Unnecessary services in enabled state.
			- Dùng các dịch vụ không cần thiết
			- Nên xóa hoặc vô hiệu hóa các dịch vụ khi không dùng đến
		4. Default accounts with their default passwords.
			- Dùng những mật khẩu mặc định từ những nhà cung cấp
			- Nên đổi mật khẩu thay vì dùng mật khẩu mặc định được cấp hoặc tạo tài khoản mới rồi xóa tài khoản mặc định
		5. Exposure of administrative or debugging notifications to general users.
			- Thông báo lỗi từ những trang quản trị đến người dùng phổ thông
		6. Misconfigured SSL certificates and encryption settings.
			- Không sử dụng phương thức mã hóa và chứng chỉ số SSL
			- Nên dùng HTTPs thay vì HTTP
		7. Misconfiguration of user roles.
			- Lỗi trong việc chia role cho từng user
			- Nên tham khảo việc chia role từ các nguồn đáng tin cậy
- Using ZAP tool (Part 2)
	- Dùng công cụ ZAP để quét lỗi website kiểm tra
	- Cài đặt cổng bắt sự kiện cho công cụ ZAP: Tools -> Local proxies -> Port
	- Lưu certificates từ Dynamic SSL Certificates
	- Thêm certificates vừa lưu vào Firefox
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/17KcXa-VFGA8SopLiawBibk6NNMHl0sVl/view?usp=sharing)