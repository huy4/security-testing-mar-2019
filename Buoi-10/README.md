# Buổi 10
## Tổng quan
- A8 (2013) – Cross-Site Request Forgery (CSRF)
## Chi tiết
- A8 (2013) - Cross-Site Request Forgery (CSRF)
	- Tấn công CSRF là dạng tấn công  làm user thực hiện các thực thi không mong muốn dựa vào xác thực hiện tại trên trình duyệt
	- Mục tiêu:
		- Thay đổi trạng thái của request
	- Đặc điểm:
		- Không đánh cắp dữ liệu
		- Trường hợp người dùng nếu tấn công CSRF thành công có thể thay trạng thái như chuyển tiền, thay đổi địa chỉ email
		- Trường hợp người quản trị hacker có thể thỏa hiệp toàn bộ quyền truy cập ứng dụng
	- Cách phòng chống:
		- Sử dụng Secret cookie/ CSRF token
		- Sử dụng thuộc tính X-CSRF-Token
## Đính kèm
- [Slide tham khảo](https://drive.google.com/file/d/14CtvbEOylhB1Ye16bWtNRUBJBbD0gwuu/view?usp=sharing)