# security-test

# Introduction

# Security Testing

### Planning  

- Expected Time: 12 buổi
- Finish day: 14/04/2019

----------------------


### Day 1: 09/03/2019

#### Today's progress:
- Introducing
- Security Midset
- Intercept & Modify Request
- Setup BurpSuite & Browser
- Hands on Labs: Bypass the client side validation
#### Throughts: 
- Bài học rất thú vị
- Được biết thêm client side và server side khắc phục những rủi ro
#### Link(s) to work:
1. [Buổi 1](./Buoi-1/README.md)

### Day 2: 10/03/2019

#### Today's progress:
- Security Test Report
- Security Test Reproduce
#### Throughts:
- Mẫu report cho rủi ro (risk) trong security cũng tương tự report cho bugs trong lập trình
- Hiểu được cách đọc report risk từ người khác, tái hiện lại và báo cáo
#### Link(s) to work:
- [Buổi 2](./Buoi-2/README.md)

### Day 3: 16/03/2019

#### Today's progress:
- Tìm hiểu về OWASP Top 10
- A1:2017-Injection
#### Throughts:
- Hiểu hơn về tấn công Injection và cách phòng chống
- Report các risk về tấn công Injection
#### Link(s) to work:
- [Buổi 3](./Buoi-3/README.md)

### Day 4: 17/03/2019

#### Today's progress:
- A2 - Broken Authentication
- Tìm hiểu và thực hành về Authentication Checklist
#### Throughts:
- Hiểu hơn về tấn công Broken Authentication
- Thực hành về Authentication checklist
#### Link(s) to work:
- [Buổi 4](./Buoi-4/README.md)

### Day 5: 23/03/2019

#### Today's progress:
- Setup OWASP Mutillidae (practice website)
- A3:2017-Sensitive Data Exposure
- A4:2017-XML External Entities (XXE)
#### Throughts:
- Biết được các thông tin nào không được phép lộ ra ngoài
- Cách phòng chống của tấn công XXE
#### Link(s) to work:
- [Buổi 5](./Buoi-5/README.md)

### Day 6: 24/03/2019

#### Today's progress:
- A5:2017-Broken Access Control
- A6:2017-Security Misconfiguration
- Using ZAP tool (Part 2)
#### Throughts:
- Độ quan trọng của phân quyền trong web app
- Sử dụng các phương thức mã hóa trên đường truyển để đảm bảo an toàn thông tin
#### Link(s) to work:
- [Buổi 6](./Buoi-6/README.md)

### Day 7: 30/03/2019

#### Today's progress:
- A7:2017-Cross-Site Scripting (XSS)
- Using Xenotix
#### Throughts:
- Tấn công XSS là rất nguy hiểm, cần xử lý các đoạn mã script khi người dùng gửi lên
- Dùng Xenotix để test XSS
#### Link(s) to work:
- [Buổi 7](./Buoi-7/README.md)

### Day 8: 31/03/2019

#### Today's progress:
- Review Session + Security Test Plan
#### Throughts:
- Củng cố kiến thức ở bài SQL Injection
- Bổ sung kiến thức đã quên ở bài Broken Authentication
#### Link(s) to work:
- [Buổi 8](./Buoi-8/README.md)

### Day 9: 06/04/2019

#### Today's progress:
- A8:2017-Insecure Deserialization
- A9:2017-Using Components with Known Vulnerabilities
- A10:2017-Insufficient Logging & Monitoring
#### Throughts:
- Hiểu được rủi ro khi xử lý serialization và deserialization không tốt
- Cách kiểm tra các lỗi của các components
- Cách ghi lại log và thông báo khi website bị tấn công
#### Link(s) to work:
- [Buổi 9](./Buoi-9/README.md)

### Day 10: 07/04/2019

#### Today's progress:
- A8 (2013) – Cross-Site Request Forgery (CSRF)
#### Throughts:
- Hiểu được cách tấn công CSRF
#### Link(s) to work:
- [Buổi 10](./Buoi-10/README.md)

### Day 11: 13/04/2019

#### Today's progress:
- A10 (2013) – Unvalidated Redirects and Forwards
#### Throughts:
- Tấn công bằng Redirects và Forwards hiện nay không phổ biến
- Dạng tấn công này dựa trên sơ ý của người dùng khi chuyển trang
#### Link(s) to work:
- [Buổi 11](./Buoi-11/README.md)

### Day 12: 14/04/2019

#### Today's progress:
- Tìm hiểu thêm một số chức năng của Burp suite
#### Throughts:
- Burp suite có tính năng `Intercepts` hay giúp quá trình scan về từng bước để quan sát rõ hơn từng bước của request
#### Link(s) to work:
- [Buổi 12](./Buoi-12/README.md)